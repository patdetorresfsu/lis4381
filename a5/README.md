# LIS4381

### Patricio Gonzalez De Torres

## Assignment 5 Requirements:

1. Basic server-side validation
2. Data table with valid and invalid test values
3. Skillset 13
4. Skillset 14
5. Skillset 15
6. Local Web Link: http://localhost/repos/lis4381/


#### README.md file should include the following items:

* Screenshot of Data Table from index.php
* Screenshot of valid and invalid values from data table
* Screenshot of Skillset 13 running
* Screenshot of Skillset 14 running
* Screenshot of Skillset 15 running
 

## Assignment Screenshots:

### Assignment 5 Server-side Validation  
*Link to local Host Repo http://localhost/repos/lis4381/a5/

*index.php* 
![Front Page](img/index.png)

*Add Pet Store Failed Validation*:
![failed](img/invalid.png)

*Process Failed Validation*:
![Success](img/failed_validation.png)

*Add Pet Store Successful Validation*:
![Success](img/valid.png)

*Process Passed Validation*:
![Success](img/passed_validation.png)

*Skillset 13 (Sphere Volume Calculator)*:
![Skillset 10](img/s13.png)

*Skillset 14 (Simple Calculator)*:
![Skillset 11](img/simple_calculator.png)

*Skillset 15 (Write Read File)*:
![Skillset 12](img/write_read_file.png)


