# LIS4381

### Patricio Gonzalez De Torres

## Assignment 4 Requirements:

1. Create a digital Portfolio website
2. Create a data validation table for a Pet Store database
3. Skillset 10
4. Skillset 11
5. Skillset 12


#### README.md file should include the following items:

* Screenshot of main Web Page
* Screenshot of Skillset 10 running
* Screenshot of Skillset 11running
* Screenshot of Skillset 12 running
* Screenshot of Successful Data Validation 
* Screenshot of Failed Data Validation  

## Assignment Screenshots:

### Assignment 4 Data Validation  
*Screenshot of Pet Store Data Validation running http://localhost*:

*Main Web Page* 
![Front Page](img/frontpage.png)

*Failed Data Validation*:
![failed](img/failscreen.png)

*Successful Data Validation*:
![Success](img/success.png)

*Skillset 10 (ArrayList)*:
![Skillset 10](img/s10.png)

*Skillset 11 (Alpha Numeric SPecial)*:
![Skillset 11](img/s11.png)

*Skillset 12 (Temperature Conversion)*:
![Skillset 12](img/s12.png)


