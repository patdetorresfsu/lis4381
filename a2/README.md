## LIS4381

## Patricio Gonzalez De Torres

### Assignment 2 Requirements:

1. Create a mobile recipe app using Android Studio
2. Skillset 1
3. Skillset 2
4. Skillset 3

#### README.md file should include the following items:

* Screenshot of First User Interface 
* Screenshot of Second User Interface
* Screenshot of Skillset 1 running
* Screenshot of Skillset 2 running
* Screenshot of Skillset 3 running



#### Assignment Screenshots:

*Screenshot of Recipe app running http://localhost*:

*First User Interface* 
![Screen 1 Screenshot](img/screen1.png)

*Second User Interface*:
![Screen 2 Screenshot](img/screen2.png)

*Skillset 1(Even or Odd)*:

![Even or Odd Skillset](img/skillset1.png)

*Skillset 2(Largest Number)*:

![Largest NUmber Skillset](img/skillset2.png)

*Skillset 3(Arrays and Loops)*:
![Animal Array Skillset](img/skillset3.png)

