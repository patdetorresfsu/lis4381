# LIS4381 - Mobile Web Application Development

## Patricio Gonzalez De Torres 
**LIS4381 Requirements:**

Course Work Links:


1. [A1 README.md](A1/README.md)
      - Install Ampps
      - Install JDK
      - Provide Screenshots of installations
      - Create Bitbucket repo
      - Complete Bitbucket tutorial(bitbucketstationslocations and myteamquotes)
      - provide git command descriptions
2. [A2 README.md](a2/README.md)

      - Create a mobile recipe app using Android Studio
      - Change color of background and text
      - Skillset 1
      - Skillset 2
      - Skillset 3
      
3. [A3 README.md](a3/README.md)

      - Create a mobile events app using Android Studio
      - Skillset 4
      - Skillset 5
      - Skillset 6
      - a3 ERD
      - a3.mwb
      - a3.sql

4. [P1 README.md](p1/README.md)

      - Create "Business Card" App Using Android Studio
      - Provide Screenshots of Running App
      - Complete Skillsets 7-9
      - Provide Screenshots of SkillSet Code Compiling

      
5. [A4 README.md](a4/README.md)

      - Create a digital Portfolio website
      - Create a data validation table for a Pet Store database
      - Skillset 10
      - Skillset 11
      - Skillset 12

6. [A5 README.md](a5/README.md)

      - Basic server-side validation
      - Data table with valid and invalid test values
      - Skillset 13
      - Skillset 14
      - Skillset 15
      
7. [P2 README.md](p2/README.md)

      -  Server-side validation
      -  Edit/Delete functionality
      -  RSS Feed