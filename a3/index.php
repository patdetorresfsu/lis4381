<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
		<meta name="author" content="Patricio Gonzalez De Torres.">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment 3</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> Develop a Pet Store database system as well as a ticket buying app for concerts and events
				</p>

				<h4>ERD Screenshot</h4>
				<img src="img/erd.png" class="img-responsive center-block" alt="erd">

				<h4>Pet Store Data table</h4>
				<img src="img/customers.png" class="img-responsive center-block" alt="datatable">

				<h4>Gif of application running</h4>
				<img src="img/myEventApp.gif" class="img-responsive center-block" alt="myeventapp">
				

				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
