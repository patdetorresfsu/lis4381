# LIS4381

### Patricio Gonzalez De Torres

## Assignment 3 Requirements:

1. Create a mobile events app using Android Studio
2. Skillset 4
3. Skillset 5
4. Skillset 6
5. ERD
6. a3.mwb
7. a3.sql

#### README.md file should include the following items:

* GIF Recording of Event App First and Second User Interfaces
* Screenshot of Skillset 4 running
* Screenshot of Skillset 5 running
* Screenshot of Skillset 6 running
* Screenshot of 10 records for each table

## Database Links: 

[a3.sql](docs/a3.sql)

[a3.mwb](docs/a3.mwb)

## Assignment Screenshots:

### Event 
*Screenshot of My Event app running http://localhost*:

*My Event Application in Action* 
![Event App Gif](img/myEventApp.gif)

*Skillset 4 (Decision Structures)*:

![Skillset 4](img/decision_structures.png)

*Skillset 5 (Nested Structures)*:

![Skillset 5](img/nested_structures.png)

*Skillset 6 (Methods)*:
![Skillset 6](img/Methods.png)

### Database
*ERD* 
![ERD Screenshot](img/erd.png)

*Data Tables*
![Pet Table](img/pet.png)
![Pet Table](img/petstore.png)
![Pet Table](img/customers.png)

