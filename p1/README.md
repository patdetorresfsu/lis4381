# LIS4381

### Patricio Gonzalez De Torres

## Project 1 Requirements:

1. Create "Business Card" App Using Android Studio
2. Provide Screenshots of Running App
3. Complete Skillsets 7-9
4. Provide Screenshots of SkillSet Code Compiling

#### README.md file should include the following items:

* Screenshots of Business Card app running
* Screenshot of Skillset 7 running
* Screenshot of Skillset 8 running
* Screenshot of Skillset 9 running

## Assignment Screenshots:

### Business Card 
*Screenshot of My Event app running http://localhost*:

*My Event Application in Action* 
![BusinessCard1](img/p1_screen1.png) 
![BusinessCard2](img/p1_screen2.png)

*Skillset 7 (Random Array Data Validation)*:
![Skillset 7](img/s7.png)

*Skillset 8 (Three Largest Numbers)*:
![Skillset 8](img/s8.png)

*Skillset 9 (Array Runtime Validation)*:
![Skillset 9](img/s9.png)