import java.util.*;

public class Methods {
    public static void getRequirements() {
        System.out.println("Developer: Patricio Gonzalez De Torres");
        System.out.println("Program searches user-entered integer w/in array of integers.");
        System.out.println("Create an array with the following values: \n 3, \n 2, \n 4, \n 99, \n -1, \n -5, \n 3, \n 7.");
        
        System.out.println();
    }

    public static void nestedStructures(){
        Scanner sc = new Scanner(System.in);

        int[] test = {3, 2, 4, 99, -1, -5, 3, 7};
        System.out.println("\nArray Length: " + test.length);

        System.out.println("Enter search value: ");
        int val = sc.nextInt();

        for (int i = 0; i < test.length; i++){
            
            if (test[i] == val) {
                System.out.println(val + " is found at index " + i);
            } else {
                System.out.println(val + " is not found at index " + i);
            }
        }
    }
}
