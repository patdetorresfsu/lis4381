import java.util.*;

public class Methods {

    public static void getRequirements() {
        System.out.println("Developer: Patricio Gonzalez De Torres");
        System.out.println("Program evaluates user-entered characters.");
        System.out.println("Use following characters: W or w \n C or c \n H or h \n N or n");
        System.out.println("Use following decision structures: if... else, and switch");
        System.out.println();
    }

    public static void getUserPhoneType(){
        String myStr = "";
        char myChar = ' ';
        Scanner sc = new Scanner (System.in);

        System.out.println("Phone types: W or w (work), \n C or c (cell), \n H or h (home), \n N or n (none). ");

        System.out.println("Enter Phone Type: ");
        myStr = sc.next().toLowerCase();
        myChar = myStr.charAt(0);

        System.out.println("\nif... else:");

        if(myChar == 'w'){
            System.out.println("Phone Type: Work");
        } else if(myChar == 'c'){
            System.out.println("Phone Type: Cell");
        } else if(myChar == 'h'){
            System.out.println("Phone Type: Work");
        } else if(myChar == 'n'){
            System.out.println("Phone Type: None");
        } else 
        System.out.println("Incorrect character entry.");

        System.out.println();
        System.out.println("Switch");
        switch (myChar){
            case 'w':
                System.out.println("Phone Type: Work.");
                break;
                case 'c':
                System.out.println("Phone Type: Cell.");
                break;
                case 'h':
                System.out.println("Phone Type: Home.");
                break;
                case 'n':
                System.out.println("Phone Type: None.");
                break;
        }
    }
}