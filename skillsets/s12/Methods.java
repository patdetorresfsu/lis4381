import java.util.*;

public class Methods {
    public static void getRequirements() {
        System.out.println("Developer: Patricio Gonzalez De Torres");
        System.out.println("Temperature Conversion Program."
        + "\nProgram converts user entered temperatures into Fahrenheit or Celsius scales. "
        + "\nNote: Upper or lower case letters ermitted. Though incorrect entries are not permitted."
        + "\nProgram continues to prompt for user entry until no longer requested."
        + "\nNote: Program does not validate numeric data (optional requirement)");
        
        System.out.println();

    }

    public static void convertTemp(){
        Scanner sc = new Scanner(System.in);
        double temperature = 0.0;
        char choice = ' ';
        char type = ' ';

        do 
        {
            System.out.println("Fahrenheit to Celsius? Type \"f\" or Celsius to Fahrenheit? Type \"c\": ");
            type =sc.next().charAt(0);
            type = Character.toLowerCase(type);
            if (type == 'f')
            {
                System.out.println("Enter temperature in Fahrenheit: ");
                temperature = sc.nextDouble();
                temperature = ((temperature - 32)*5)/9;
                System.out.println("Temperature in Celsius = " + temperature);    
            }
            else if (type == 'c'){
                System.out.println("ENter temperature in Celsius: ");
                temperature = sc.nextDouble();
                temperature = (temperature * 9/5) + 32;
                System.out.println(("Temperature in Fahrenheit = " + temperature));
            }
            else {
                System.out.println("Incorrect entry. Please try again.");
            }
            System.out.println("\nDo you want to convert a temperature? (y or n) ");
            choice = sc.next().charAt(0);
            choice = Character.toLowerCase(choice);
        }
        while (choice == 'y');

        

        System.out.println("Thank you for using our Temperature Conversion Program.");
    }
}
