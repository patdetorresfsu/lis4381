import java.util.Scanner;
import java.util.ArrayList;

public class Methods {
        static final Scanner sc = new Scanner(System.in);
        public static void getRequirements() {
            System.out.println("Developer: Patricio Gonzalez De Torres");
            System.out.println("Program populates ArrayList of strings with user entered animal type values."
            + "\nExamples: Polar bear, Guinea Pig, dog, cat, bird."
            + "\nProgram continues to collect user-entered values until user types in 'n'."
            + "\nProgram displays ArrayList values after each iteration, as well as size.");
            
            System.out.println();
    
        }
        
        public static void createArrayList(){
            Scanner sc = new Scanner(System.in);
            ArrayList<String> obj = new ArrayList<String>();
            String myStr = "";
            String choice = "y";
            int num = 0;

            while (choice.equals("y")){
                System.out.println("Enter animal type: ");
                myStr = sc.nextLine();
                obj.add(myStr);
                num = obj.size();
                System.out.println("ArrayList elements: " + obj + "\nArrayList Size = " + num);
                System.out.println("\nContinue? Enter y or n: ");
                choice = sc.next().toLowerCase();
                sc.nextLine();
            }
        }
 }
    

