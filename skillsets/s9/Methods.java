import java.util.Scanner;

import javax.swing.plaf.synth.SynthSpinnerUI;
public class Methods {
    static final Scanner sc = new Scanner(System.in);
    public static void getRequirements() {
        System.out.println("Developer: Patricio Gonzalez De Torres");
        System.out.println("1) Program creates array size at run-time."
        + "\n2) Program displays array size."
        + "\n3) Program rounds sum and average of numbers to two decimal places."
        + "\n4) Numbers *must* be float data type, not double.");
        
        System.out.println();

    }

    public static int validateArraySize(){
        int arraySize = 0;
        System.out.print("Please enter array size: ");
        while(!sc.hasNextInt()){
            System.out.println("Not valid integer!\n");
            sc.next();
            System.out.print("Please Try again. Enter Array Size: ");
        }
        arraySize = sc.nextInt();
        System.out.println();

        return arraySize;
    }

    public static void calculateNumbers(int arraySize){
        float sum = 0.0f;
        float avg = 0.0F;

        System.out.print("PLease enter " + arraySize + " numbers.\n");
        float numsArray[] = new float[arraySize];

        for(int i = 0; i < arraySize; i++){
            System.out.print("Enter num " + (i + 1) + ": ");

            while(!sc.hasNextFloat()){
                System.out.println("Not valid number!");
                sc.next();
                System.out.println("Please try again. Enter num " + (i + 1) + ": ");
            }
            numsArray[i] = sc.nextFloat();
            sum = sum + numsArray[i];
        }
        avg = sum / arraySize;
        System.out.println("Numbers entered: ");
        for(int i = 0; i < numsArray.length; i++)
            System.out.println(numsArray[i]+ " ");
            printNumbers(sum, avg);        
        
    }

    public static void printNumbers(float sum, float avg){
        System.out.println("Sum: " + String.format("%.2f", sum));
        System.out.println("Average: " + String.format("%.2f", avg));
    }    
}


