
import java.util.Scanner;

import jdk.dynalink.beans.StaticClass;

public class Methods {
    public static void getRequirements() {
        System.out.println("Developer: Patricio Gonzalez De Torres");
        System.out.println("Program prompts user for firt name and age, then prints results.");
        System.out.println("Create four methods from the following requirements:");
        System.out.println("1) getRequirements(): Void method displays program requirements. "
        + "\n 2) getUserInput(): Void method prompts for user input, \n then calls out two methods: myVoidMethod() and myValueReturningMethod()."
        + "\n 3) myVoidMethod(): \n"
        + "\ta. Accepts two arguments: String and int. \n"
        + "\tb. Prints user's first name and age."
        + "\n 4) myValueReturningMethod(): \n "
        + "\ta. Accepts two arguments: String and int. \n "
        + "\tb. Returns String containing first name and age.");
        
        System.out.println();
    }

    public static void getUserInput(){
        String firstName = "";
        int userAge = 0;
        String myStr = "";
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter first name: ");
        firstName = sc.next();

        System.out.println("Enter age: ");
        userAge = sc.nextInt();

        System.out.println();

        System.out.print("void method call: ");
        myVoidMethod(firstName, userAge);

        System.out.print("value-returning method call: ");
        myStr = myValueReturningMethod(firstName, userAge);
        System.out.println(myStr);
    }
    
    public static void myVoidMethod(String first, int age){
        System.out.println(first + " is " + age);
        return;
    }

    public static String myValueReturningMethod(String first, int age){
        return first + " is " + age;
    }
    }

