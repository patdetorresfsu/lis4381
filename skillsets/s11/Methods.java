import java.util.*;

public class Methods {
    static final Scanner sc = new Scanner(System.in);
        public static void getRequirements() {
            System.out.println("Developer: Patricio Gonzalez De Torres");
            System.out.println("Program determines whether user entered alpha, numeric or special character."
            + "\nReferences: "
            + "\nASCII Background: https://en.wikipedia.org/wiki/ASCII."
            + "\nASCII Character Table: https://www.ascii-code.com/"
            + "\nLookup Tables: https://www.lookuptables.com/");
            
            System.out.println();

        }

    public static void determineChar(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter Character: ");

        char ch = sc.next().charAt(0);

        if((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z'))
        {
            System.out.println(ch + " is alpha SCII value: " + (int) ch);
        }
        else if (ch >='0' && ch <= '9')
        {
            System.out.println(ch + " is a numeric ASCII value: " + (int) ch );
        }
        else {
            System.out.println(ch + " is special character ASCII Value. " + (int)ch);
        }

    }
}

