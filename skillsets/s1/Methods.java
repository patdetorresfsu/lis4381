
import java.util.Scanner;
public class Methods {
    
    public static void getRequirements() {
        System.out.println("Developer: Patricio Gonzalez De Torres");
        System.out.println("Program evaluates whether the integer is even or odd");
        System.out.println("Note: Program does not check for non-numeric characters or non-integer values");
        System.out.println();
    }

    public static void evaluateNumber() {
        int x = 0;
        System.out.print("Enter integer: ");
        Scanner sc = new Scanner(System.in);
        x = sc.nextInt();

        if (x % 2 == 0) {
            System.out.println(x + " is an even number.");
        } else {
            System.out.println(x + " is an odd number.");
        }
    }
}
