# LIS4381

### Patricio Gonzalez De Torres

## Project 2 Requirements:

1. Server-side validation
2. Edit/Delete functionality
3. RSS Feed


#### README.md file should include the following items:

* Course title, your name, assignment requirement, as per A1
* Screenshots as per below examples
* ink to local Host Repo http://localhost/repos/lis4381/p2/
 

## Assignment Screenshots:

*Carousel*:
![Carousel](img/carousel.png)

*index.php* 
![Index.php](img/index.php.png)

*Edit Petstore*:
![edit](img/edit_petstore.png)

*Failed Validation*:
![Fail](img/failed_validation.png)

*Passed Validation*:
![Success](img/passed_validation.png)

*Delete Record Prompt*:
![Delete Prompt](img/delete_record_prompt.png)

*Successfully Delete Record*:
![Success Delete](img/delete_success.png)

*RSS Feed*:
![RSS Feed](img/rss_feed.png)


