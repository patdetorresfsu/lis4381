<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
		<meta name="author" content="Patricio Gonzalez De Torres.">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - RSS Link</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>
		<h1>ABC News</h1>
		<h2>http://feeds.abcnews.com/abcnews/topstories</h2>
		<?php 
		include_once("../global/nav.php"); 
		
		$html = "";
		$publisher = "ABC";
		$url = "http://feeds.abcnews.com/abcnews/topstories";

		$html .= '<h2' .$publisher . '</h2>';
		$html .= $url;

		$rss = simplexml_load_file($url);
		$count = 0;
		$html .= '<ul>';
		foreach($rss->channel->item as $item)
		{
			$count++;
			if($count > 10)
			{
				break;
			}
			$html .= '<li><a href ="'.htmlspecialchars($item->link).'">'. htmlspecialchars($item->title) . '</a></br/>';
			$html .= htmlspecialchars($item->description) . '</br/>';
			$html .= htmlspecialchars($item->pubDate) . '</li></br/>';
		}
		$html .= '</ul>';
		
		print $html;
		?>
		
		
  </body>
</html>
